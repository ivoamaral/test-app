@extends('adminlte::page')

@section('title', 'Test App - Employees')

@section('content_header')
    <h1>Employees</h1>
@stop

@section('content')

    @if ($errors->any())

    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> There were some problems with your input.</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        
    @endif

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Edit employee</h3>
        </div>
        
        <form class="form-horizontal" action="{{ route('employees.update', $employee->id) }}" method="POST">
            {{csrf_field()}}
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="inputFName" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $employee->first_name}}" name="first_name" class="form-control" id="inputFName" placeholder="First Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLName" class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $employee->last_name}}" name="last_name" class="form-control" id="inputLName" placeholder="Last Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputCompany" class="col-sm-2 control-label">Company</label>
                    <div class="col-sm-10">
                        <select name="company_id" class="form-control" id="inputCompany">
                        @foreach ($companies as $company)    
                            <option value="{{ $company->id }}"
                            @if ($company->id == $employee->company_id)    
                                selected
                            @endif
                            >{{ $company->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" value="{{ $employee->email }}" name="email" class="form-control" id="inputEmail" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $employee->phone }}" name="phone" class="form-control" id="inputPhone" placeholder="Phone">
                    </div>
                </div>
            </div>
              <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
              <!-- /.box-footer -->
        </form>
    </div>
@stop