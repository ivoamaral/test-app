@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Employees</h1>
@stop

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            {{ $message }}
        </div>
    @endif

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title pull-left">List of Employees</h3>
            <div class="box-tools">
                <a href="{{ route('employees.create') }}" class="btn btn-block btn-default pull-right ">Add New</a>
            </div>
        </div>
        
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($employees as $employee)
                    <tr>
                        <td>{{ $employee->id  }}</td>
                        <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                        <td>{{ $employee->company->name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>
                            <form action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{--@method('DELETE')--}}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-default btn-md">Edit</a>
                                <button type="submit" class="btn btn-default btn-md ">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
              </tbody>
            </table>
        </div>
    
        <div class="box-footer clearfix">
            <div class="pull-right">{!! $employees->links() !!}</div>
        </div>
    </div>
@stop