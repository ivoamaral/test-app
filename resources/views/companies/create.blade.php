@extends('adminlte::page')

@section('title', 'Test App - Companies')

@section('content_header')
    <h1>Companies</h1>
@stop

@section('content')

    @if ($errors->any())

    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> There were some problems with your input.</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
        
    @endif

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Add new company</h3>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
        <form class="form-horizontal" action="{{ route('companies.store') }}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="inputName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputWebsite" class="col-sm-2 control-label">Website</label>
                    <div class="col-sm-10">
                        <input type="text" name="website" class="form-control" id="inputWebsite" placeholder="Website">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLogo" class="col-sm-2 control-label">Logo</label>
                    <div class="col-sm-10">
                        <input type="file" name="logo" class="form-control" id="inputLogo">
                        <!-- <input type="text" name="logo" class="form-control" id="inputLogo" placeholder="Logo"> -->
                    </div>
                </div>
            </div>
              <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
              <!-- /.box-footer -->
        </form>
    </div>
@stop