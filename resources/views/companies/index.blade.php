@extends('adminlte::page')

@section('title', 'Test App - Companies')

@section('content_header')
    <h1>Companies</h1>
@stop

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            {{ $message }}
        </div>
    @endif

    <div class="box">

        <div class="box-header with-border">
            <h3 class="box-title pull-left">List of Companies</h3>
            <div class="box-tools">
                <a href="{{ route('companies.create') }}" class="btn btn-block btn-default pull-right ">Add New</a>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->email }}</td>
                        <td>{{ $company->website }}</td>
                        <td>
                            <form action="{{ route('companies.destroy',$company->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{--@method('DELETE')--}}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('companies.edit', $company->id) }}" class="btn btn-default btn-md">Edit</a>
                                <button type="submit" class="btn btn-default btn-md ">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
              </tbody>
            </table>
        </div>
    
        <div class="box-footer clearfix">
            <div class="pull-right">{!! $companies->links() !!}</div>
        </div>
        
    </div>

    
@stop